# -*- coding: utf-8 -*-
"""
Created on Thu Nov 12 09:37:13 2015

@author: lollar
"""
import pandas as pd
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.metrics import log_loss

train = pd.read_csv("transformed_train.csv")
validation = pd.read_csv("transformed_validation.csv")
test = pd.read_csv("transformed_test.csv")

X = train.drop(['TripType', 'VisitNumber'], axis=1)
X = pd.get_dummies(X, columns=['Weekday'], prefix="Weekday")
ylabels = train.TripType.map(lambda x: "TripType_" + str(x))

Xvalidation = validation.drop(['TripType', 'VisitNumber'], axis=1)
Xvalidation = pd.get_dummies(Xvalidation, columns=['Weekday'], prefix="Weekday")
yvalidationLabels = validation.TripType.map(lambda x: "TripType_" + str(x))

Xtest = test.drop(['VisitNumber'], axis=1)
Xtest = pd.get_dummies(Xtest, columns=['Weekday'], prefix="Weekday")
## Gradient Boosted Trees


gb = GradientBoostingClassifier(n_estimators=20)

gb.fit(X,ylabels)

pred = gb.predict_proba(Xvalidation)

log_loss(yvalidationLabels, pred)

## predict and write submission file

testPred = gb.predict_proba(Xtest)

submission = pd.DataFrame(testPred, columns=gb.classes_)

import re

def atoi(text):
    return int(text) if text.isdigit() else text

def natural_keys(text):
    '''
    alist.sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    (See Toothy's implementation in the comments)
    '''
    return [ atoi(c) for c in re.split('(\d+)', text) ]
    

cols = submission.columns.tolist()
cols.sort(key=natural_keys)

submission[cols].head()

submission.loc[:, "VisitNumber"] = test.VisitNumber

cols = submission.columns.tolist()
cols = cols[-1:] + cols[:-1]

submission = submission[cols]

submission.to_csv("submission.csv",index=False)