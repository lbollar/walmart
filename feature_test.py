import pandas as pd
import numpy as np
from sklearn import cross_validation as cv
from scipy import sparse

train = pd.read_csv("/home/lbollar/kaggle/walmart/train.csv", dtype={'Upc':object, 'FinelineNumber':object, 'TripType':object})

visits = train.VisitNumber.unique()


train_visits, test_visits = cv.train_test_split(visits, test_size=0.4, random_state=4)

train_cv = train[train.VisitNumber.isin(train_visits)]
validation = train[train.VisitNumber.isin(test_visits)]

fineNums = train_cv.FinelineNumber.unique()

countFLN = train_cv.groupby(['FinelineNumber']).count().sort('Weekday',ascending=False).VisitNumber
topFLN = countFLN[countFLN > 100].reset_index().FinelineNumber.tolist()

def sparseFeatures2(dataframe, trainFeatures):
  
  # Features are for passed data frame, unique Features are for proper column
  # placement in sparse matrix, they are from the original train matrix
  features = dataframe.FinelineNumber.tolist()
  uniqueVisits = dataframe.VisitNumber.unique().tolist()
  uniqueFeatures = trainFeatures.unique().tolist()  
    
  featureDict = dict(enumerate(uniqueFeatures))
  featureDict = {v: k for k, v in featureDict.items()}
  
  visitsDict = dict(enumerate(uniqueVisits))
  visitsDict = {v: k for k, v in visitsDict.items()}
  
  sparseMatrix = sparse.lil_matrix((57404, 5047))  
  
  for i, visit in enumerate(dataframe.VisitNumber.tolist()):
    row = visitsDict.get(visit)
    col = featureDict.get(features[i], -1)
    
    if col != -1:
      sparseMatrix[row, col] = sparseMatrix[row, col] + 1  
  
  return(sparseMatrix.tocsr())
  

trainSparseMatrix = sparseFeatures2(train_cv, train_cv.FinelineNumber)
validationSparseMatrix = sparseFeatures2(validation, train_cv.FinelineNumber)

