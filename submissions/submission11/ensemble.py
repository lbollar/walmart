import pandas as pd

sub10 = pd.read_csv('/home/lbollar/kaggle/walmart/submissions/submission10/submission.csv')
sub9 = pd.read_csv('/home/lbollar/kaggle/walmart/submissions/submission9/submission.csv')

visits = sub10.VisitNumber

sub10 = sub10.drop('VisitNumber', axis='columns')
sub9 = sub9.drop('VisitNumber', axis='columns')

columns = sub10.columns.tolist()

combined = (sub9.as_matrix() + sub10.as_matrix())/2

combined = pd.DataFrame(combined)
combined.columns = columns

submission = pd.concat([visits,combined], axis=1)

submission.to_csv('/home/lbollar/kaggle/walmart/submissions/submission11/submission.csv', index=False)