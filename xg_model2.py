import xgboost as xgb
import pandas as pd 
import re
from scipy import sparse


def createTrainMatrix(dataframe, sparseFeatures, sparseColumnNames = None):
  dataframe = dataframe.reset_index()
  Xdf = dataframe.drop(['TripType', 'VisitNumber'], axis=1)
  Xdf = pd.get_dummies(Xdf, columns=['Weekday'], prefix="Weekday")
  ylabels = pd.factorize(dataframe.TripType, sort=True)
  
  X = sparse.csr_matrix(Xdf)
  X = sparse.hstack([X, sparseFeatures]).tocsr()
  
  if sparseColumnNames == None:  
    DMatrix = xgb.DMatrix(X, ylabels[0])  
  else:
    featureNames = Xdf.columns.tolist() + sparseColumnNames
    print(len(featureNames))
    print(X.shape[1])
    DMatrix = xgb.DMatrix(X, ylabels[0], feature_names=featureNames)    
    
  return(DMatrix, ylabels[1])
  
def createTestMatrix(dataframe, sparseFeatures):
  dataframe = dataframe.reset_index()
  X = dataframe.drop(['VisitNumber'], axis=1)
  X = pd.get_dummies(X, columns=['Weekday'], prefix="Weekday")
  
  X = sparse.csr_matrix(X)
  X = sparse.hstack([X, sparseFeatures]).tocsr()
  
  DMatrix = xgb.DMatrix(X)  
  
  return(DMatrix)
  

trainX, yIndices = createTrainMatrix(trainMatrix, trainSparseMatrix, colNames)
validationX, validationIndices = createTrainMatrix(validationMatrix, validationSparseMatrix, colNames)
testX = createTestMatrix(reorderedTestMatrix, testSparseMatrix)

param = {}

param['objective'] = 'multi:softprob'
param['eta'] = 0.01
param['max_depth'] = 6
param['silent'] = 1
param['num_class'] = 38
param['eval_metric'] = 'mlogloss'
param['seed'] = 3
param['early_stopping_rounds'] = 100
param['colsample_bytree'] = .5
param['nthread'] = 4

watchlist = [ (trainX,'train'), (validationX, 'test') ]
num_round = 3000

bst = xgb.train(param, trainX, num_round, watchlist)

pred = bst.predict(testX)

classes = yIndices

submission = pd.DataFrame(pred, columns = classes.map(lambda x: "TripType_" + str(x)))
submission['VisitNumber'] = testMatrix.reset_index().VisitNumber


## following for correct header order


def atoi(text):
    return int(text) if text.isdigit() else text

def natural_keys(text):
    '''
    alist.sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    (See Toothy's implementation in the comments)
    '''
    return [ atoi(c) for c in re.split('(\d)', text) ]
    
cols = submission.columns.tolist()
cols.sort(key=natural_keys)

cols = cols[-1:] + cols[:-1]

submission = submission[cols]

submission.to_csv("/home/lbollar/kaggle/walmart/submissions/submission12/submission.csv", index=False)