import xgboost as xgb
import pandas as pd 
import re
import os

os.chdir("/home/lbollar/kaggle/walmart/featureEngineering/featureSet3/")

train = pd.read_csv("train.csv")
validation = pd.read_csv("validation.csv")
test = pd.read_csv("transformed_test.csv")


X = train.drop(['TripType', 'VisitNumber'], axis=1)
X = pd.get_dummies(X, columns=['Weekday'], prefix="Weekday")
ylabels = pd.factorize(train.TripType, sort=True)

Xvalidation = validation.drop(['TripType', 'VisitNumber'], axis=1)
Xvalidation = pd.get_dummies(Xvalidation, columns=['Weekday'], prefix="Weekday")
yValidationLabels = pd.factorize(validation.TripType, sort=True)

Xtest = test.drop(['VisitNumber'], axis=1)
Xtest = pd.get_dummies(Xtest, columns=['Weekday'], prefix="Weekday")

## Remove Characters required for DMatrix
## ylabels = ylabels.map(lambda x: int(re.sub("[^0-9]", "", x)))


train_DMatrix = xgb.DMatrix(X, label = ylabels[0])
validation_DMatrix = xgb.DMatrix(Xvalidation, label = yValidationLabels[0])
test_DMatrix = xgb.DMatrix(Xtest)

param = {}

param['objective'] = 'multi:softprob'
param['eta'] = 0.1
param['max_depth'] = 8
param['silent'] = 1
param['num_class'] = 38
param['eval_metric'] = 'mlogloss'
param['seed'] = 3

watchlist = [ (train_DMatrix,'train'), (validation_DMatrix, 'test') ]
num_round = 10

bst = xgb.train(param, train_DMatrix, num_round, watchlist)

pred = bst.predict(test_DMatrix)

classes = ylabels[1]

submission = pd.DataFrame(pred, columns = classes.map(lambda x: "TripType_" + str(x)))
submission['VisitNumber'] = test.VisitNumber


## following for correct header order


def atoi(text):
    return int(text) if text.isdigit() else text

def natural_keys(text):
    '''
    alist.sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    (See Toothy's implementation in the comments)
    '''
    return [ atoi(c) for c in re.split('(\d)', text) ]
    
cols = submission.columns.tolist()
cols.sort(key=natural_keys)

cols = cols[-1:] + cols[:-1]

submission = submission[cols]

submission.to_csv("/home/lbollar/kaggle/walmart/submissions/submission6/submission.csv", index=False)