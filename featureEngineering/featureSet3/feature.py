import pandas as pd
import numpy as np
from sklearn import cross_validation as cv

# ------------------------------------------------------
#     Read in and split train and validation sets up
# ------------------------------------------------------

train = pd.read_csv("~/kaggle/walmart/train.csv", dtype={'Upc':object, 'FinelineNumber':object, 'TripType':object})

visits = train.VisitNumber.unique()


train_visits, test_visits = cv.train_test_split(visits, test_size=0.4, random_state=4)

train_cv = train[train.VisitNumber.isin(train_visits)]
validation = train[train.VisitNumber.isin(test_visits)]


    

tripTypes = train.TripType.unique()
tripTypes.sort()

tripTypesColumnHeader = ["TripType_" + str(num) for num in tripTypes]

train_cv.loc[:,'Returns'] = train_cv['ScanCount'].map(lambda x: 1 if x < 0 else 0)


Departments = [x for x in train_cv.DepartmentDescription.unique() if str(x) != 'nan']

for dep in Departments:
  train_cv.loc[:,dep] = train_cv['DepartmentDescription'].map(lambda x:1 if x == dep else 0)

# Extract first number of Upc
beginningUpc = train_cv['Upc'].map(lambda x: str(x)[0]).unique()

for begin in beginningUpc:
  train_cv.loc[:, 'UpcBegin_' + begin] = train_cv['Upc'].map(lambda x: 1 if str(x)[0] == begin else 0)

# Group and Aggregate
visitGroupby = train_cv.groupby(['VisitNumber','Weekday','TripType'])

train_cv_grouped_output = pd.DataFrame()
train_cv_grouped_output['ReturnBoolean'] = visitGroupby['Returns'].aggregate(np.max)

# compute the number of returned items and Total Items
train_cv_grouped_output['ReturnSum'] = visitGroupby['Returns'].aggregate(np.sum)

train_cv_grouped_output['TotalItems'] = visitGroupby['TripType'].size()

for dep in Departments:
  train_cv_grouped_output[dep] = visitGroupby[dep].aggregate(np.sum)

# compute number of Upc's by first number

for begin in beginningUpc:
  train_cv_grouped_output['UpcBegin_' + begin] = visitGroupby['UpcBegin_' + begin].aggregate(np.sum)

train_cv_grouped_output.to_csv("train.csv")


# -----------------------------------------------
#    Process validation Data
# -----------------------------------------------

validation.loc[:, 'Returns'] = validation['ScanCount'].map(lambda x: 1 if x < 0 else 0)

for dep in Departments:
    validation.loc[:, dep] = validation['DepartmentDescription'].map(lambda x: 1 if x == dep else 0)
    
# Extract first number of Upc
beginningUpc = train_cv['Upc'].map(lambda x: str(x)[0]).unique()

for begin in beginningUpc:
  validation.loc[:, 'UpcBegin_' + begin] = validation['Upc'].map(lambda x: 1 if str(x)[0] == begin else 0)
  
validationVisitGroupby = validation.groupby(['VisitNumber', 'Weekday', 'TripType'])

validation_grouped_output = pd.DataFrame()
validation_grouped_output['ReturnBoolean'] = validationVisitGroupby['Returns'].aggregate(np.max)

validation_grouped_output['ReturnSum'] = validationVisitGroupby['Returns'].aggregate(np.sum)
validation_grouped_output['TotalItems'] = validationVisitGroupby['TripType'].size()

for dep in Departments:
    validation_grouped_output[dep] = validationVisitGroupby[dep].aggregate(np.sum)

# compute number of Upc's by first number

for begin in beginningUpc:
  validation_grouped_output['UpcBegin_' + begin] = validationVisitGroupby['UpcBegin_' + begin].aggregate(np.sum)


validation_grouped_output.to_csv("validation.csv")

# --------------------------------------------------
#    Process test data
# --------------------------------------------------

test = pd.read_csv("~/kaggle/walmart/test.csv", dtype={'Upc':object, 'FinelineNumber':object})

test.loc[:, 'Returns'] = test['ScanCount'].map(lambda x: 1 if x < 0 else 0)

for dep in Departments:
    test.loc[:, dep] = test['DepartmentDescription'].map(lambda x: 1 if x == dep else 0)
  
for begin in beginningUpc:
  test.loc[:, 'UpcBegin_' + begin] = test['Upc'].map(lambda x: 1 if str(x)[0] == begin else 0)
  
  
testVisitGroupby = test.groupby(['VisitNumber', 'Weekday'])

test_grouped_output = pd.DataFrame()
test_grouped_output['ReturnBoolean'] = testVisitGroupby['Returns'].aggregate(np.max)

test_grouped_output['ReturnSum'] = testVisitGroupby['Returns'].aggregate(np.sum)
test_grouped_output['TotalItems'] = testVisitGroupby['VisitNumber'].size()

for dep in Departments:
    test_grouped_output[dep] = testVisitGroupby[dep].aggregate(np.sum)
    
# compute number of Upc's by first number

for begin in beginningUpc:
  test_grouped_output['UpcBegin_' + begin] = testVisitGroupby['UpcBegin_' + begin].aggregate(np.sum)


test_grouped_output.to_csv("transformed_test.csv")