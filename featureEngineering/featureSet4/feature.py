import pandas as pd
import numpy as np
from sklearn import cross_validation as cv
from scipy import sparse

train = pd.read_csv("/home/lbollar/kaggle/walmart/train.csv", dtype={'Upc':object, 'FinelineNumber':object, 'TripType':object})
test = pd.read_csv("/home/lbollar/kaggle/walmart/test.csv", dtype={'Upc':object, 'FinelineNumber':object})

visits = train.VisitNumber.unique()


train_visits, test_visits = cv.train_test_split(visits, test_size=0.4, random_state=4)

train_cv = train[train.VisitNumber.isin(train_visits)]
validation = train[train.VisitNumber.isin(test_visits)]

Departments = [x for x in train_cv.DepartmentDescription.unique() if str(x) != 'nan']

countFLN = train_cv.groupby(['FinelineNumber']).count().sort('Weekday',ascending=False).VisitNumber
topFLN = countFLN[countFLN > 100].reset_index().FinelineNumber.tolist()

def createFeatures(dta, Departments, destFile = None):
  dta.loc[:,'Returns'] = dta['ScanCount'].map(lambda x: 1 if x < 0 else 0)


  for dep in Departments:
    dta.loc[:,dep] = dta['DepartmentDescription'].map(lambda x:1 if x == dep else 0)

  # Extract first number of Upc
  beginningUpc = train_cv['Upc'].map(lambda x: str(x)[0]).unique()

  for begin in beginningUpc:
    dta.loc[:, 'UpcBegin_' + begin] = dta['Upc'].map(lambda x: 1 if str(x)[0] == begin else 0)
    
      
  # Group and Aggregate
  visitGroupby = dta.groupby(['VisitNumber','Weekday','TripType'])

  output = pd.DataFrame()
  output['ReturnBoolean'] = visitGroupby['Returns'].aggregate(np.max)

  # compute the number of returned items and Total Items
  output['ReturnSum'] = visitGroupby['Returns'].aggregate(np.sum)

  output['TotalItems'] = visitGroupby['TripType'].size()

  for dep in Departments:
    output[dep] = visitGroupby[dep].aggregate(np.sum)
    
  
  if destFile != None:
    output.to_csv(destFile)

    
  return(output)

def createTestFeatures(dta, Departments, destFile = None):
  dta.loc[:,'Returns'] = dta['ScanCount'].map(lambda x: 1 if x < 0 else 0)


  for dep in Departments:
    dta.loc[:,dep] = dta['DepartmentDescription'].map(lambda x:1 if x == dep else 0)

  # Extract first number of Upc
  beginningUpc = train_cv['Upc'].map(lambda x: str(x)[0]).unique()

  for begin in beginningUpc:
    dta.loc[:, 'UpcBegin_' + begin] = dta['Upc'].map(lambda x: 1 if str(x)[0] == begin else 0)
    
      
  # Group and Aggregate
  visitGroupby = dta.groupby(['VisitNumber','Weekday'])

  output = pd.DataFrame()
  output['ReturnBoolean'] = visitGroupby['Returns'].aggregate(np.max)

  # compute the number of returned items and Total Items
  output['ReturnSum'] = visitGroupby['Returns'].aggregate(np.sum)

  output['TotalItems'] = visitGroupby['VisitNumber'].size()

  for dep in Departments:
    output[dep] = visitGroupby[dep].aggregate(np.sum)
    
  
  if destFile != None:
    output.to_csv(destFile)

    
  return(output)
  
def sparseFeatures(dataframe, trainFeatures):
  '''
  Features are for passed data frame, unique Features are for proper column
  placement in sparse matrix, they are from the original train matrix
  
  trainFeatures: Fineline column of original train dataframe to prevent
                 leakage of features and correct column order/number of columns
  '''  
  features = dataframe.FinelineNumber.tolist()
  uniqueVisits = dataframe.VisitNumber.unique().tolist()
  uniqueFeatures = trainFeatures.unique().tolist()  
    
  featureDict = dict(enumerate(uniqueFeatures))
  featureDict = {v: k for k, v in featureDict.items()}
  
  visitsDict = dict(enumerate(uniqueVisits))
  visitsDict = {v: k for k, v in visitsDict.items()}
  
  sparseMatrix = sparse.lil_matrix((len(uniqueVisits), len(uniqueFeatures)))  
  
  for i, visit in enumerate(dataframe.VisitNumber.tolist()):
    row = visitsDict.get(visit)
    col = featureDict.get(features[i], -1)
    
    if col != -1:
      sparseMatrix[row, col] += 1
  
  return(sparseMatrix.tocsr())
  
def sparseFeatures2(dataframe, trainFeatures):
  '''
  Features are for passed data frame, unique Features are for proper column
  placement in sparse matrix, they are from the original train matrix
  
  trainFeatures: list of unique Fineline Numbers
  '''  
  
  features = dataframe.FinelineNumber.tolist()
  uniqueVisits = dataframe.VisitNumber.unique().tolist()

    
  featureDict = dict(enumerate(trainFeatures))
  featureDict = {v: k for k, v in featureDict.items()}
  
  visitsDict = dict(enumerate(uniqueVisits))
  visitsDict = {v: k for k, v in visitsDict.items()}
  
  sparseMatrix = sparse.lil_matrix((len(uniqueVisits), len(trainFeatures)))  
  
  for i, visit in enumerate(dataframe.VisitNumber.tolist()):
    row = visitsDict.get(visit)
    col = featureDict.get(features[i], -1)
    
    if col != -1:
      sparseMatrix[row, col] += 1
  
  return(sparseMatrix.tocsr())
  

trainSparseMatrix = sparseFeatures(train_cv, train_cv.FinelineNumber)
validationSparseMatrix = sparseFeatures(validation, train_cv.FinelineNumber)
testSparseMatrix = sparseFeatures(test, train_cv.FinelineNumber)

trainMatrix = createFeatures(train_cv, Departments)
validationMatrix = createFeatures(validation, Departments)
testMatrix = createTestFeatures(test, Departments)